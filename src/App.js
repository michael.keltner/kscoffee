import React, { useRef} from 'react';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core';
import emailjs from '@emailjs/browser';
import User from './orderComponents/User'
import Mug from './orderComponents/Mug';
import Drink from './orderComponents/Drink'
import Delivery from  './orderComponents/Delivery'
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
console.log(process.env.REACT_APP_API_SECRET);

const App = () => {

  const useStyles = makeStyles(theme => ({
    body: {
      minheight: '100vh',
      paddingtop: '25px'
    },
    root: {
      width:'200px',
      height:'200px',
      border: '1px solid red',
      overflow:'hidden',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      padding: theme.spacing(2),
  
      '& .MuiTextField-root': {
        paddingtop: '10px',
        margin: theme.spacing(2),
        width: '300px',
      },
      '& .MuiButtonBase-root': {
        margin: theme.spacing(2),
      },
      textField: {
          width: '100%',
          marginLeft: 'auto',
          marginRight: 'auto',            
          paddingBottom: 0,
          marginTop: 10,
          fontWeight: 800
      },
      input: {
          color: 'white'
      }
    },
  }));

  const classes = useStyles();
  const form = useRef();
  const handleSubmit = e => {
    e.preventDefault();
    const drinks = ['Americano','Cappuccino','Espresso','Flat','Latte', 'Macchiato', 'Mocha', 'Make it a Double Shot', 'Add Chocolate Shavings'];
    let drinkOrder = (drinks.map((drink)=>(form.current[drink].checked?'1 ' + drink +' ':''))).toString();
    let hiddenOrderField = document.getElementById('drinkOrder');
    hiddenOrderField.value = drinkOrder;
    console.log(form.current);
    console.log(form.current.fromName.value, form.current.drinkOrder.value, form.current.deliverTime.value, form.current.mugSelection.value, form.current.cost.value);

    emailjs.sendForm('service_ja1sqen', 'template_vq0y16j', form.current, 'TLndMlqFsJ9w2xq7w')
       .then((result) => {
        alert("Message Sent, We will get back to you shortly", result.text);
    },
(error) => {
alert("An error occurred, Please try again", error.text);
});
  };
//service_ja1sqen
  return (
    <div className="App">
    <form ref={form} onSubmit={handleSubmit}>
    <Stack spacing={3}>
      < Typography variant="subtitle1" gutterBottom component="div">
        K's Coffee Order Form
      </Typography>
      <User/>
      <Delivery/>
      <Mug/>
      <Drink/>
      <Button variant="contained" color="primary" onClick={handleSubmit} >Place Order</Button>
    </Stack>
    </form>
    </div>
  );
};

export default App;
