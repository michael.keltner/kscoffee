import React, { useRef, useState } from 'react';
import { makeStyles } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import emailjs from '@emailjs/browser';
import Mug from './orderComponents/Mug';
import Drink from './orderComponents/Drink'
console.log(process.env.REACT_APP_API_SECRET);

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.spacing(2),

    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '300px',
    },
    '& .MuiButtonBase-root': {
      margin: theme.spacing(2),
    },
    textField: {
        width: '90%',
        marginLeft: 'auto',
        marginRight: 'auto',            
        paddingBottom: 0,
        marginTop: 0,
        fontWeight: 500
    },
    input: {
        color: 'white'
    }
  },
}));

const Form = ({ handleClose }) => {
  const classes = useStyles();
  const form = useRef();
  // create state variables for each input
  /*
  const [fromName, fromName] = useState('');
  const [coffeeType, coffeeType] = useState('');
  const [amount, amount] = useState('');
  const [deliveryTime, deliveryTime] = useState('');
  const [location, location] = useState('');
  const [phone, phone] = useState('');
 //*/

  const handleSubmit = e => {
    e.preventDefault();
   
    console.log(form.current.fromName, form.current.coffeeType, form.current.amount, form.current.deliveryTime, form.current.cup, form.current.phone);
    /*
    emailjs.sendForm('service_ja1sqen', 'template_vq0y16j', form.current, 'TLndMlqFsJ9w2xq7w')
        .then((result) => {
        alert("Message Sent, We will get back to you shortly", result.text);
    },
    
(error) => {
alert("An error occurred, Please try again", error.text);
});
//*/
    handleClose();
  };
//service_ja1sqen

  return (
    <form ref={form} className={classes.root} onSubmit={handleSubmit}>
      <input type="text" name="fromName" placeholder='Name'/>
      <Drink/>
      <input type="text" name="deliveryTime" placeholder='Delivery Time'/>
      <Button variant="contained" type="submit" value="Send" >Place Order</Button>

    </form>
  );
};

export default Form;