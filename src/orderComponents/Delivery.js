import * as React from 'react';

import TextField from '@mui/material/TextField';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';


export default function MaterialUIPickers() {
  const [value, setValue] = React.useState(new Date());
  const handleChange = (newValue) => {
    setValue(newValue);
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DateTimePicker
          label="Delivery Date & Time"
          value={value}
          name = "deliveryTime"
          variant="standard"
          closeOnSelect={false}
          disablePast={true}
          onChange={handleChange}
          renderInput={(params) => <TextField {...params} />}
        />
        <input type="hidden" name="deliverTime" value={value}/>
    </LocalizationProvider>
  );
}
