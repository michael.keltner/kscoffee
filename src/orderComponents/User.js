
import * as React from 'react';
import TextField from '@mui/material/TextField';

export default function MaterialUIPickers() {
  return (
    <TextField 
    id="fromName" 
    size="normal" 
    name="fromName" 
    label="Name" 
    variant="outlined"
    />
  );
}
