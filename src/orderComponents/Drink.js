import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Switch from '@mui/material/Switch';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import TextField from '@material-ui/core/TextField';

export default function CheckboxListSecondary() {
  const [checked, setChecked] = React.useState([1]);
  const drinks = new Map();
  drinks.set('Americano',1.0);
  drinks.set('Cappuccino',1.5);
  drinks.set('Espresso',1.0);
  drinks.set('Flat', 1.0);
  drinks.set('Latte', 1.5);
  drinks.set('Macchiato',1.0);
  drinks.set('Mocha',2.5);
  drinks.set('Make it a Double Shot', 0.25);
  drinks.set('Add Chocolate Shavings', 'Free')
  const handleToggle = (value) => () => {

    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }
    setChecked(newChecked);
    let total =0;
    newChecked.forEach(checkedDrink => {
        if (isNaN(checkedDrink)){
          if (drinks.get(checkedDrink) !== 'Free'){
              total += parseFloat(drinks.get(checkedDrink))
          }
        }
    });

    var totalCostField = document.getElementById('totalCost');
    totalCostField.value = '$' + total;
    console.log(total, newChecked, "set value to:", value, drinks.get(value), totalCostField, totalCostField.value)


  };
  drinks.forEach((price, drink) => {console.log( drinks.get(drink))});
  return (
    <List dense sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
      {['Americano','Cappuccino','Espresso','Flat','Latte', 'Macchiato', 'Mocha', 'Make it a Double Shot', 'Add Chocolate Shavings'].map((drink) => {
        const labelId = `checkbox-list-secondary-label-${drink}`;
        return (
           
          <ListItem
            key={drink}
            secondaryAction={
                <Switch
                edge="end"
                name={drink}
                onChange={handleToggle(drink)}
                checked={checked.indexOf(drink) !== -1}
                inputProps={{
                  'aria-labelledby': labelId,
                }}
              />
             
            }
            disablePadding
          >
            
            <ListItemButton>
              <ListItemAvatar>
                <Avatar
                  alt={`${drink}`}
                  src={`/images/${drink.replace(/\s/g, '')}.png`}
                />
              </ListItemAvatar>
              <ListItemText id={labelId} primary={`${drink}`}
              secondary={
                <React.Fragment>
                  <Typography
                    sx={{ display: 'inline' }}
                    component="span"
                    variant="body2"
                    color="text.primary"
                  >
                  $ 
                  </Typography>
                  {drinks.get(drink)}
                </React.Fragment>
              }
              
              />
            </ListItemButton>
          </ListItem> 
        );
      })}
       <Typography
        sx={{ display: 'inline' }}
        component="span"
        variant="body1"
        color="text.primary"
        >    
            <TextField 
                id="totalCost" 
                size="medium" 
                name="cost" 
                variant="standard"
                disabled
            />
         </Typography>
         <input type="hidden" name="drinkOrder" id="drinkOrder"/>
    </List>
    
  );
}
