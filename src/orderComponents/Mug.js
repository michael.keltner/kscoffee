import * as React from 'react';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import LocalCafeOutlinedIcon from '@mui/icons-material/LocalCafeOutlined';
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined';
import TimeToLeaveOutlinedIcon from '@mui/icons-material/TimeToLeaveOutlined';
import Typography from '@mui/material/Typography';

export default function ColorToggleButton() {
  const [alignment, setAlignment] = React.useState('web');

  const handleChange = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  return ( 
     <> < Typography variant="subtitle1" gutterBottom component="div">
      How do you want to enjoy your drink?
    </Typography>
    <ToggleButtonGroup
      color="primary"
      value={alignment}
      exclusive
      onChange={handleChange}
    >
 
        <ToggleButton value="KMug"><LocalCafeOutlinedIcon />With Ms. K's mug</ToggleButton>
        <ToggleButton value="Pick Up Mug"><FavoriteBorderOutlinedIcon />With my favorite mug</ToggleButton>
        <ToggleButton value="ToGos"><TimeToLeaveOutlinedIcon />In a to-go cup</ToggleButton>
        <input type="hidden" name="mugSelection" value={alignment}/>
      </ToggleButtonGroup></>
     
  );
}
